import { Router } from "express";
import Meeting, { IMeeting } from "../models/Meeting";

const router = Router();
let request = require("request");
const zoomAPI = "https://api.zoom.us/v2";
const token = "Bearer " + "{PUT HERE YOUR TOKEN}";

router.get("/", async (req, res) => {
  const todaysDate = getTodaysDate();
  const url =
    zoomAPI + "/metrics/meetings" + "?from=" + todaysDate + "?to=" + todaysDate;

  const options = {
    method: "GET",
    url: url,
    headers: {
      authorization: token,
    },
  };

  request(
    options,
    async function (error: string | undefined, response: any, body: any) {
      if (error) {
        throw new Error(error);
      } else {
        const data = JSON.parse(body);

        if (!data.meetings.length) {
          return res.json([]);
        } else {
          let liveMeetings: IMeeting[] = data.meetings.map((meeting: any) => {
            return {
              _id: meeting.id,
              participants: meeting.participants,
              isLive: true,
            };
          });

          const meetingIds = liveMeetings.map(
            (meeting: IMeeting) => meeting._id
          );

          const existingMeetings = await Meeting.find({
            _id: { $in: meetingIds },
          });

          // Update existing meetings only if needed
          liveMeetings.forEach(async (liveMeeting) => {
            let existingMeeting = existingMeetings.find(
              (meeting) => meeting._id === liveMeeting._id
            );
            if (
              existingMeeting &&
              (!existingMeeting.isLive ||
                existingMeeting.participants !== liveMeeting.participants)
            ) {
              await existingMeeting
                .set("participants", liveMeeting.participants)
                .set("isLive", true)
                .save();
            }
          });

          const newMeetings = liveMeetings.filter(
            (meeting) =>
              !existingMeetings.some(
                (existMeeting) => existMeeting._id === meeting._id
              )
          );

          // Add only new meetings to the DB
          await Meeting.insertMany(newMeetings).catch((e) => {
            console.log(
              "An error occurred while trying to insert new meetings to the DB..."
            );
          });

          return res.json(
            liveMeetings.map((meeting) => {
              meeting.id = meeting._id;
              const { _id, ...rest } = meeting;
              return rest;
            })
          );
        }
      }
    }
  );
});

router.put("/end", async (req, res) => {
  const { meetingId } = req.body;

  const options = {
    url: zoomAPI + "/meetings/" + meetingId + "/status",
    headers: {
      authorization: token,
    },
    json: true,
    body: {
      action: "end",
    },
  };

  request.put(options, async (err: any, res: any, body: any) => {
    if (err) {
      console.log("An error occurred while trying to end meeting...");
    } else {
      // Update Meeting DB status as well
      await Meeting.updateOne(
        {
          _id: meetingId,
        },
        { $set: { isLive: false } }
      );
    }

    return res.statusCode === 204;
  });
});

function getTodaysDate() {
  const today = new Date();

  return (
    String(today.getDate()).padStart(2, "0") +
    "/" +
    String(today.getMonth() + 1).padStart(2, "0") +
    "/" +
    today.getFullYear()
  );
}

export default router;
