import { Router } from "express";
import meetings from "./meetings";

const routes = Router();

routes.use("/meetings", meetings);

export default routes;
