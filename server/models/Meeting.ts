import mongoose, { Schema, Document } from "mongoose";

export interface IMeeting extends Document {
  participants: number;
  _id: number;
  isLive: boolean;
}

const MeetingSchema: Schema = new Schema({
  participants: { type: Number, required: true },
  isLive: Boolean,
  _id: Number,
});

export default mongoose.model<IMeeting>("Meeting", MeetingSchema);
