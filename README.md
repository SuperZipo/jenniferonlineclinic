# Jennifer's Online Clinic

## Installation

1. Set up a token for zoom API

- Go to the next path: "server/routes/meetings.ts".
- Next, go to line 7 and replace "{PUT HERE YOUR TOKEN}" with the needed token.

2. Install server's node modules and run it (cd from root directory)

```bash
cd server 
npm install
npm run start
```

3. Install client's node modules and run it (cd from root directory)

```bash
cd client
npm install
npm run start
```

4. Go to chrome to http://localhost:3000/
