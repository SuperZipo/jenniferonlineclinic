import "../css/OnlineClinic.css";
import { DataGrid, GridColDef } from "@material-ui/data-grid";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { selectMeetings } from "../app/reducers/meetingsSlice";
import { Typography } from "@material-ui/core";
import { IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { endMeeting } from "../app/reducers/meetingsSlice";

interface OnlineClinicProps {}

export default function OnlineClinic(props: OnlineClinicProps) {
  const liveMeetings = useAppSelector(selectMeetings);
  const dispatch = useAppDispatch();

  const EndMeetingButton = ({ meetingId }: { meetingId: number }) => {
    const handleEndMeeting = () => {
      dispatch(endMeeting(meetingId));
    };

    return (
      <IconButton
        color="secondary"
        aria-label="add an alarm"
        onClick={handleEndMeeting}
      >
        <CloseIcon />
      </IconButton>
    );
  };

  const columns: GridColDef[] = [
    { field: "id", headerName: "ID", width: 120 },
    {
      field: "participants",
      headerName: "Participants",
      width: 200,
    },
    {
      field: "endMeeting",
      headerName: "End Meeting",
      sortable: false,
      width: 140,
      renderCell: (params) => {
        return (
          <div
            className="d-flex justify-content-between align-items-center"
            style={{ cursor: "pointer" }}
          >
            <EndMeetingButton meetingId={params.row.id} />
          </div>
        );
      },
    },
  ];

  return (
    <>
      <Typography variant="h2" component="h2" gutterBottom>
        Jennifer's Online Clinic
      </Typography>
      <div
        style={{
          height: 400,
          width: "80%",
          backgroundColor: "#ffffff",
          opacity: "90%",
        }}
      >
        <DataGrid
          rows={liveMeetings}
          columns={columns}
          disableSelectionOnClick
        />
      </div>
    </>
  );
}
