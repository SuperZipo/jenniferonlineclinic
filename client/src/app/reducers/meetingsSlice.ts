import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import type { RootState } from "../store";
import axios from "axios";
import Meeting from "./../../types/Meeting";

interface MeetingState {
  meetings: Meeting[];
  isDataLoaded: boolean;
}

const initialState: MeetingState = {
  meetings: [],
  isDataLoaded: false,
};

export const fetchMeetings = createAsyncThunk(
  "meetings/fetchMeetings",
  async () => {
    const response = await axios.get("http://localhost:8000/meetings/");
    return response.data;
  }
);

export const endMeeting = createAsyncThunk(
  "meetings/endMeeting",
  async (meetingId: number) => {
    const response = await axios.put("http://localhost:8000/meetings/end", {
      meetingId: meetingId,
    });

    return response.data;
  }
);

export const meetingsSlice = createSlice({
  name: "meetings",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchMeetings.fulfilled, (state, action) => {
      state.isDataLoaded = true;
      state.meetings = [...action.payload];
    });
    builder.addCase(endMeeting.pending, (state, action) => {
      const meetingId = action.meta.arg;

      state.meetings = state.meetings.filter(
        (meeting) => meeting.id !== meetingId
      );
    });
  },
});

export const selectMeetings = (state: RootState) => state.meetings.meetings;
export const selectIsMeetingsLoaded = (state: RootState) =>
  state.meetings.isDataLoaded;

export default meetingsSlice.reducer;
