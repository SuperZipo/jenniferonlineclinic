export default interface Meeting {
  participants: number;
  id: number;
  isLive: boolean;
}
