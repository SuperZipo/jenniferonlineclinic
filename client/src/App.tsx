import "./App.css";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import OnlineClinic from "./components/OnlineClinic";
import {
  fetchMeetings,
  selectIsMeetingsLoaded,
} from "./app/reducers/meetingsSlice";
import { CircularProgress } from "@material-ui/core";

function App() {
  const isMeetingsLoaded = useAppSelector(selectIsMeetingsLoaded);
  const dispatch = useAppDispatch();
  dispatch(fetchMeetings());

  return <>{isMeetingsLoaded ? <OnlineClinic /> : <CircularProgress />}</>;
}

export default App;
